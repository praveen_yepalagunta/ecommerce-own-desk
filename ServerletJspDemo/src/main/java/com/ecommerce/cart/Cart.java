package com.ecommerce.cart;

public class Cart {
	int cart_id;
	int quantity;
	int prod_id;
	int user_id;
	
	 
	@Override
	public String toString() {
		return "Cart [cart_id=" + cart_id + ", quantity=" + quantity + ", prod_id=" + prod_id + ", user_id=" + user_id
				+ "]";
	}
	
	
	public Cart(int cart_id, int quantity, int prod_id, int user_id) {
		super();
		this.cart_id = cart_id;
		this.quantity = quantity;
		this.prod_id = prod_id;
		this.user_id = user_id;
	}


	public int getCart_id() {
		return cart_id;
	}


	public void setCart_id(int cart_id) {
		this.cart_id = cart_id;
	}


	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public int getProd_id() {
		return prod_id;
	}


	public void setProd_id(int prod_id) {
		this.prod_id = prod_id;
	}


	public int getUser_id() {
		return user_id;
	}


	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}


	public int getOrder_id() {
		// TODO Auto-generated method stub
		return 0;
	}


 
 
	

}
